﻿using ICSHP_Asteroids.GameObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace ICSHP_Asteroids
{
    delegate void MoveCallback();

    class Game
    {
        public PlayerShip Player { get; private set; }
        public bool GameOver { get; private set; }

        public int Score { get;private set; }

        private Ship enemyShip;
        private List<Bullet> playerBullets;
        private List<Bullet> enemyBullets;
        private List<Asteroid> asteroids;
        private MoveCallback moves;
        private Canvas gameCanvas;
        private int level;
        private Random random;
        private bool addLife;
        private int shipTimer;
        private int shipCounterInLevel;
        private double difficulty;
        private int scoreFromAsteroid;

        private const int BaseScoreFromAsteroid = 100;
        private const int ScoreToNextLife = 10000;
        private const int ShipInterval = 1500;
        private const int MaxShipsInLevel = 10;



        public Game(Canvas gameCanvas, double difficulty)
        {
            this.gameCanvas = gameCanvas;
            playerBullets = new List<Bullet>();
            enemyBullets = new List<Bullet>();
            asteroids = new List<Asteroid>();
            random = new Random();
            addLife = false;
            shipTimer = (int)(ShipInterval / difficulty);
            shipCounterInLevel = 0;
            this.difficulty = difficulty;
            scoreFromAsteroid = (int)(BaseScoreFromAsteroid * difficulty);

            Player = new PlayerShip(gameCanvas, new Point(gameCanvas.ActualWidth/2, gameCanvas.ActualHeight/2), difficulty);
            moves = Player.Move;
            level = 1;
            NextLevel();
        }

        public void Move(object sender, EventArgs e)
        {
            moves();
            RedrawObjects();
            PlayerFire();
            CreateEnemyShip();
            EnemyFire();
            CheckBulletsLifetime(playerBullets);
            CheckBulletsLifetime(enemyBullets);
            DetectCollisions();
        }

        private void CreateEnemyShip()
        {
            if (enemyShip == null && shipTimer < 0 && shipCounterInLevel <= MaxShipsInLevel)
            {
                enemyShip = new Ship(gameCanvas, new Point(0, random.Next(0, (int)gameCanvas.ActualHeight)), difficulty);
                moves += enemyShip.Move;
                shipTimer = (int)(ShipInterval / difficulty);
                shipCounterInLevel++;
            }
            shipTimer--;
        }

        private void EnemyFire()
        {
            if (enemyShip != null && enemyShip.FireCountdown <= 0)
            {
                Bullet bullet = enemyShip.Fire(Player.Center);
                moves += bullet.Move;
                enemyBullets.Add(bullet);
            }

        }

        private void PlayerFire()
        {
            if (Player.IsFiring)
            {
                Bullet bullet = Player.Fire();
                if (bullet != null)
                {
                    moves += bullet.Move;
                    playerBullets.Add(bullet);
                }
            }
        }

        private void CheckBulletsLifetime(List<Bullet> bullets)
        {
            foreach (var item in bullets.ToList())
            {
                if (item.ToDestroy)
                {
                    DestroyBullet(item, bullets);
                }
            }
        }

        private void RedrawObjects()
        {
            gameCanvas.Children.Clear();
            gameCanvas.Children.Add(Player.Shape);
            foreach (var item in playerBullets)
            {
                gameCanvas.Children.Add(item.Shape);
            }
            foreach (var item in asteroids)
            {
                gameCanvas.Children.Add(item.Shape);
            }
            if (enemyShip != null)
            {
                gameCanvas.Children.Add(enemyShip.Shape);
            }

            foreach (var item in enemyBullets)
            {
                gameCanvas.Children.Add(item.Shape);

            }
        }

        private void DestroyEnemyShip()
        {
            moves -= enemyShip.Move;
            gameCanvas.Children.Remove(enemyShip.Shape);
            enemyShip = null;

        }

        private void DestroyPlayer()
        {
            Player.Destroy();
            if (Player.Lives <= 0)
            {
                GameOver = true;
            }
        }
        private void DestroyAsteroid(Asteroid asteroid)
        {
            moves -= asteroid.Move;
            gameCanvas.Children.Remove(asteroid.Shape);

            switch (asteroid.Type)
            {
                case AsteroidType.Big:
                    BreakAsteroid(asteroid.Center, AsteroidType.Moderate);
                    Score += scoreFromAsteroid;
                    break;
                case AsteroidType.Moderate:
                    BreakAsteroid(asteroid.Center, AsteroidType.Small);
                    Score += scoreFromAsteroid / 2;
                    break;
                case AsteroidType.Small:
                    Score += scoreFromAsteroid / 4;
                    break;
            }
            asteroids.Remove(asteroid);
            if (asteroids.Count == 0)
            {
                NextLevel();
            }
            AddLife();

        }

        private void AddLife()
        {
            if (Score % ScoreToNextLife > 2 * scoreFromAsteroid)
            {
                addLife = true;
            }
            if (addLife && Score % ScoreToNextLife < scoreFromAsteroid)
            {
                Player.Lives++;
                addLife = false;
            }
        }

        private void BreakAsteroid(Point center, AsteroidType type)
        {
            for (int i = 0; i < 2; i++)
            {
                Asteroid asteroid = new Asteroid(gameCanvas, center, type, difficulty);
                moves += asteroid.Move;
                asteroids.Add(asteroid);
            }
        }

        private void DestroyBullet(Bullet bullet, List<Bullet> bullets)
        {
            moves -= bullet.Move;
            gameCanvas.Children.Remove(bullet.Shape);
            bullets.Remove(bullet);
        }

        private void DetectCollisions()
        {
            DetectBulletAsteroidCollisions(playerBullets);
            DetectBulletAsteroidCollisions(enemyBullets);
            DetectPlayerCollision();
            DetectEnemyCollision();
        }

        private void DetectEnemyCollision()
        {
            if (enemyShip != null)
            {
                foreach (var asteroid in asteroids.ToList())
                {
                    if (CollisionDetected(asteroid, enemyShip))
                    {
                        DestroyEnemyShip();
                        return;
                    }

                }

                foreach (var bullet in playerBullets.ToList())
                {
                    if (CollisionDetected(bullet, enemyShip))
                    {
                        DestroyEnemyShip();
                        return;
                    }
                }
            }

        }

        private void DetectPlayerCollision()
        {
            if (Player.ImmortalityCountdown <= 0)
            {
                foreach (var asteroid in asteroids.ToList())
                {
                    if (CollisionDetected(asteroid, Player))
                    {
                        DestroyPlayer();
                        return;
                    }

                }

                foreach (var bullet in enemyBullets.ToList())
                {
                    if (CollisionDetected(bullet, Player))
                    {
                        DestroyPlayer();
                        DestroyBullet(bullet, enemyBullets);
                        return;
                    }
                }
            }
        }

        private void DetectBulletAsteroidCollisions(List<Bullet> bullets)
        {
            foreach (var bullet in bullets.ToList())
            {
                foreach (var asteroid in asteroids.ToList())
                {
                    if (CollisionDetected(asteroid, bullet))
                    {
                        DestroyBullet(bullet, bullets);
                        DestroyAsteroid(asteroid);
                        break;
                    }
                }
            }
        }

        private bool CollisionDetected(GameObject object1, GameObject object2)
        {
            double distanceX = object1.Center.X - object2.Center.X;
            double distanceY = object1.Center.Y - object2.Center.Y;
            if (Math.Sqrt(Math.Pow(distanceX, 2) + Math.Pow(distanceY, 2)) <= object1.Size + object2.Size)
            {
                IntersectionDetail collides = object1.Shape.RenderedGeometry.FillContainsWithDetail(object2.Shape.RenderedGeometry);
                if (collides != IntersectionDetail.Empty && collides != IntersectionDetail.NotCalculated)
                {
                    return true;
                }
            }
            return false;
        }
        private void NextLevel()
        {
            if (level % 2 == 1)
            {
                GenerateAsteroid(AsteroidType.Moderate);
            }
            for (int i = 0; i < 2 + level / 2; i++)
            {
                GenerateAsteroid(AsteroidType.Big); ;
            }
            level++;
            shipCounterInLevel = 0;
        }


        private void GenerateAsteroid(AsteroidType type)
        {
            Asteroid asteroid = new Asteroid(gameCanvas, GenerateRandomCenter((int)gameCanvas.ActualWidth, (int)gameCanvas.ActualHeight), type, difficulty);          
            moves += asteroid.Move;
            asteroids.Add(asteroid);
        }


        private Point GenerateRandomCenter(int maxWidth, int maxHeight)
        {
            int x = random.Next(0, maxWidth);
            int y = random.Next(0, maxHeight);
            double distanceX = x - Player.Center.X;
            double distanceY = y - Player.Center.Y;
            if (Math.Sqrt(Math.Pow(distanceX, 2) + Math.Pow(distanceY, 2)) <= 100)
            {
                return GenerateRandomCenter(maxWidth, maxHeight);
            }
            return new Point(x, y);

        }
    }
}
