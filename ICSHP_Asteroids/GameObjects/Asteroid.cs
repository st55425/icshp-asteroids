﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace ICSHP_Asteroids.GameObjects
{

    public enum AsteroidType {Big, Moderate ,Small}

    class Asteroid : GameObject
    {
        public AsteroidType Type { get; private set; }

        public static Random random = new Random();

        PointCollection shapePoints;

        public Asteroid(Canvas parent, Point center, AsteroidType type, double difficulty) : base(parent, center, difficulty)
        {
            Type = type;
            double xMovement = random.NextDouble();
            double yMovement = random.NextDouble();
            if (random.NextDouble() >= 0.5)
            {
                xMovement *= -1;
            }
            if (random.NextDouble() >= 0.5)
            {
                yMovement *= -1;
            }
            Movement = new Vector(xMovement, yMovement);
            if (type == AsteroidType.Moderate)
            {
                Movement *= 2;
            }
            else if (type == AsteroidType.Small)
            {
                Movement *= 3;
            }
            Movement *= difficulty;
            CreateShape();
            Draw();
        }

        public override void Move()
        {
            Center += Movement;
            JumpToOtherSide();
            Draw();
        }

        protected override void Draw()
        {
            Shape.Points.Clear();
            foreach (var point in shapePoints)
            {
                Shape.Points.Add(new Point(Center.X + point.X, Center.Y + point.Y));
            }
            Shape.Stroke = new SolidColorBrush(Colors.LightGray);
            Shape.Fill = new SolidColorBrush(Colors.Transparent);
            Shape.StrokeThickness = 2;
        }

        private void CreateShape()
        {
            shapePoints = new PointCollection();

            switch (Type)
            {
                case AsteroidType.Big:
                    Size = 50;
                    break;
                case AsteroidType.Moderate:
                    Size = 30;
                    break;
                case AsteroidType.Small:
                    Size = 15;
                    break;
                default:
                    Size = 50;
                    break;
            }
            int granularity = 20;
            int minVary = 25;
            int maxVary = 75;
            for (double angle = 0; angle < 2 * Math.PI; angle += 2 * Math.PI / granularity)
            {
                int angleVaryPc = random.Next(minVary, maxVary);
                double angleVaryRadians = (2 * Math.PI / granularity) * angleVaryPc / 100;
                double angleFinal = angle + angleVaryRadians - (Math.PI / granularity);
                double pointRadius = Size + random.Next(Size/5) - Size/5;
                double x = Math.Sin(angleFinal) * pointRadius;
                double y = -Math.Cos(angleFinal) * pointRadius;
                shapePoints.Add(new Point(x, y));
            }
        }

    }
}
