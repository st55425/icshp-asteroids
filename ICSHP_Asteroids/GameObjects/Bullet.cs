﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace ICSHP_Asteroids.GameObjects
{
    class Bullet : GameObject
    {
        private const int StartSpeed = 7;
        private const int TimeToLive = 100;

        private GameObject shooter;

        private int remainingTime;

        
        public Bullet(Canvas parent, Point center, double angle, GameObject shooter, double difficulty) : base(parent, center, difficulty)
        {
            Size = 1;
            this.shooter = shooter;
            remainingTime = TimeToLive;
            double xMove = Math.Sin(Math.PI * angle / 180) * StartSpeed;
            double yMove = Math.Cos(Math.PI * angle / 180) * StartSpeed;
            Movement = new Vector(xMove, -yMove);
            if (shooter is PlayerShip)
            {
                Movement += shooter.Movement;
            }
            else
            {
                remainingTime = (int)(TimeToLive * difficulty)*3;
                Movement *= difficulty / 2;
            }
            
            Draw();
        }


        public override void Move()
        {
            Center += Movement;
            JumpToOtherSide();
            Draw();
            remainingTime--;
            if (remainingTime <= 0)
            {
                ToDestroy = true;
            }
        }

        protected override void Draw()
        {
            Shape.Points.Clear();
            Shape.Points.Add(new Point(Center.X - 1, Center.Y -1));
            Shape.Points.Add(new Point(Center.X + 1, Center.Y - 1));
            Shape.Points.Add(new Point(Center.X + 1, Center.Y + 1));
            Shape.Points.Add(new Point(Center.X - 1, Center.Y + 1));
            Shape.StrokeThickness = 1;
            if (shooter.GetType() == typeof(PlayerShip))
            {
                Shape.Stroke = new SolidColorBrush(Colors.Red);
                Shape.Fill = new SolidColorBrush(Colors.Red);
            }
            else
            {
                Shape.Stroke = new SolidColorBrush(Colors.Cyan);
                Shape.Fill = new SolidColorBrush(Colors.Cyan);
            }
        }
    }
}
