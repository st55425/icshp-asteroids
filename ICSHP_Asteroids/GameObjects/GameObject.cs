﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;

namespace ICSHP_Asteroids.GameObjects
{
    abstract class GameObject
    {
        public Canvas Parent { get;protected set; }

        public Point Center { get;protected set; }

        public Vector Movement { get;protected set; }

        public Polygon Shape { get; protected set; }

        public bool ToDestroy { get; set; }

        public int Size { get; protected set; }

        protected double difficulty;

        protected GameObject(Canvas parent, Point center, double difficulty)
        {
            Parent = parent;
            Center = center;
            Movement = new Vector(0, 0);
            Shape = new Polygon();
            ToDestroy = false;
            this.difficulty = difficulty;
        }

        protected abstract void Draw();
        public abstract void Move();
        protected void JumpToOtherSide()
        {
            if (Center.X < 0)
            {
                Center = new Point(Parent.ActualWidth, Center.Y);
            }
            if (Center.X > Parent.ActualWidth)
            {
                Center = new Point(0, Center.Y);
            }
            if (Center.Y < 0)
            {
                Center = new Point(Center.X, Parent.ActualHeight);
            }
            if (Center.Y > Parent.ActualHeight)
            {
                Center = new Point(Center.X, 0);
            }
        }

    }
}
