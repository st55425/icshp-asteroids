﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace ICSHP_Asteroids.GameObjects
{
    class PlayerShip : GameObject
    {


        public int Angle { get;private set; }

        public bool IsRotatingClockwise { get; set; }
        public bool IsRotatingCounterClockwise { get; set; }

        public bool IsFiring { get; set; }

        public bool Accelerate { get; set; }

        public int ImmortalityCountdown { get;private set; }

        public int FireCountdown { get; private set; }

        public int Lives { get;set; }

        private Vector acceleration;

        public static Vector ShipSize = new Vector(24, 30);
        private const double DragFactor = 0.015;
        private const double AccelerationFactor = 0.15;

        public PlayerShip(Canvas parent, Point center, double difficulty) : base(parent, center, difficulty)
        {
            Size = (int)Math.Max(ShipSize.X, ShipSize.Y);
            Angle = 0;
            IsRotatingClockwise = false;
            IsRotatingCounterClockwise = false;
            Accelerate = false;
            acceleration = new Vector(0, 0);
            Lives = 3;
            ImmortalityCountdown = 100;

        }

        private void Rotate()
        {
            
            if (IsRotatingClockwise && !IsRotatingCounterClockwise)
            {
                Angle += 5;
                if (Angle == 360)
                {
                    Angle = 0;
                }
            }
            if (!IsRotatingClockwise && IsRotatingCounterClockwise)
            {
                Angle -= 5;
                if (Angle == -360)
                {
                    Angle = 0;
                }
            }
            RotateTransform rotateTransform2 =
            new RotateTransform(Angle);
            rotateTransform2.CenterX = Center.X;
            rotateTransform2.CenterY = Center.Y;
            Shape.RenderTransform = rotateTransform2;
            
        }

        private void AccelerateShip()
        {
            acceleration.X = Math.Sin(2 * Math.PI * ((double)Angle / 360))*AccelerationFactor;
            acceleration.Y = Math.Cos(2 * Math.PI * ((double)Angle / 360))*-AccelerationFactor; 

            Movement += acceleration;
        }

        protected override void Draw()
        {
            int xTop = 0;
            int yTop = -(int)ShipSize.Y / 2;
            int yIndent = 10;

            Shape.Points.Clear();
            Shape.Points.Add(new Point(Center.X + xTop, Center.Y + yTop));
            Shape.Points.Add(new Point(Center.X + xTop + ShipSize.X / 2, Center.Y + yTop + ShipSize.Y));
            Shape.Points.Add(new Point(Center.X + xTop, Center.Y + yTop + ShipSize.Y - yIndent));
            Shape.Points.Add(new Point(Center.X + xTop - ShipSize.X / 2, Center.Y + yTop + ShipSize.Y));
            if (ImmortalityCountdown >0)
            {
                Shape.Stroke = new SolidColorBrush(Colors.Red);
            }
            else
            {
                Shape.Stroke = new SolidColorBrush(Colors.LightCyan);
            }
            Shape.Fill = new SolidColorBrush(Colors.Transparent);
            Shape.StrokeThickness = 2;
            
            Rotate();
        }

        public override void Move()
        {
            if (Accelerate)
            {
                AccelerateShip();
            }

            Movement *= (1 - DragFactor);
            Center += Movement;
            JumpToOtherSide();
            Draw();
            if (FireCountdown >0)
            {
                FireCountdown--;
            }
            if (ImmortalityCountdown > 0)
            {
                ImmortalityCountdown--;
            }

        }

        public Bullet Fire()
        {
            if (FireCountdown <=0 )
            {
                Point top = new Point
                {
                    X = Center.X + Math.Sin(Math.PI * Angle / 180) * ShipSize.X / 2,
                    Y = Center.Y + Math.Cos(Math.PI * Angle / 180) * -ShipSize.Y / 2
                };
                Bullet bullet = new Bullet(Parent, top, Angle, this, difficulty);
                FireCountdown = 10;
                return bullet;
            }
            return null;
        }

        public void Destroy()
        {
            Center = new Point(Parent.ActualWidth / 2, Parent.ActualHeight / 2);
            acceleration = new Vector(0, 0);
            Movement = new Vector(0, 0);
            Lives--;
            Angle = 0;
            ImmortalityCountdown = 50;
        }
    }
}
