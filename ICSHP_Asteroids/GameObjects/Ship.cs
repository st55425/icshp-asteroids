﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace ICSHP_Asteroids.GameObjects
{
    class Ship : GameObject
    {

        public int FireCountdown { get; private set; }

        private const int FiringInterval = 150;

        public Ship(Canvas parent, Point center, double difficulty) : base(parent, center, difficulty)
        {
            Size = 24;
            Movement = new Vector(1, 0);
            FireCountdown = FiringInterval;
        }

        public override void Move()
        {
            Center += Movement;
            JumpToOtherSide();
            Draw();
            FireCountdown--;
        }

        protected override void Draw()
        {
            Shape.Points.Clear();
            Shape.Points.Add(new Point(Center.X + Size * 2 / 3, Center.Y));
            Shape.Points.Add(new Point(Center.X - Size, Center.Y));
            Shape.Points.Add(new Point(Center.X - Size * 2 / 3, Center.Y + Size / 3));
            Shape.Points.Add(new Point(Center.X + Size * 2 / 3, Center.Y + Size / 3));
            Shape.Points.Add(new Point(Center.X + Size, Center.Y));
            Shape.Points.Add(new Point(Center.X + Size * 2 / 3, Center.Y - Size / 3));
            Shape.Points.Add(new Point(Center.X - Size / 3, Center.Y - Size / 3));
            Shape.Points.Add(new Point(Center.X - Size / 6, Center.Y - Size * 2 / 3));
            Shape.Points.Add(new Point(Center.X + Size / 6, Center.Y - Size * 2 / 3));
            Shape.Points.Add(new Point(Center.X + Size / 3, Center.Y - Size / 3));
            Shape.Points.Add(new Point(Center.X - Size * 2 / 3, Center.Y - Size / 3));
            Shape.Points.Add(new Point(Center.X - Size, Center.Y));

            Shape.Stroke = new SolidColorBrush(Colors.DarkCyan);
            Shape.Fill = new SolidColorBrush(Colors.Transparent);
            Shape.StrokeThickness = 2;

        }

        public Bullet Fire(Point target)
        {
            Point shotPlace;
            if (target.X > Center.X)
            {
                shotPlace = new Point(Center.X + Size * 2 / 3, Center.Y);
            }
            else
            {
                shotPlace = new Point(Center.X - Size, Center.Y);
            }

            Bullet bullet = new Bullet(Parent, shotPlace, ConputeAngle(shotPlace, target), this, difficulty);
            FireCountdown = FiringInterval;
            return bullet;
        }



        private double ConputeAngle(Point ShotPlace, Point target)
        {
            Vector horizontal = new Vector(1, 0);
            Vector shotTrack = new Vector(target.X -ShotPlace.X, target.Y - ShotPlace.Y);

            double dotProduct = horizontal.X * shotTrack.X + horizontal.Y * shotTrack.Y;
            double cosinus = dotProduct / (horizontal.Length * shotTrack.Length);
            if (ShotPlace.Y - target.Y >= 0)
            {
                return -Math.Acos(cosinus) / Math.PI * 180 + 90;
            }
            return Math.Acos(cosinus) / Math.PI * 180 + 90;
        }
    }
}
