﻿using ICSHP_Asteroids.GameObjects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace ICSHP_Asteroids
{

    
    /// <summary>
    /// Interakční logika pro MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private DispatcherTimer timer;
        private Game game;
        private List<ResultNode> results;
        
        public MainWindow()
        {
            InitializeComponent();
            EditingCommands.ToggleInsert.Execute(null, nicknameTextBox);

            results = new List<ResultNode>();
            if (File.Exists("results.csv"))
            {
                try
                {
                    using (FileStream fs = File.OpenRead(@"results.csv"))
                    {
                        StreamReader sr = new StreamReader(fs);
                        string line;
                        while ((line = sr.ReadLine()) != null)
                        {
                            string[] tmp = line.Split(';');
                            int score;
                            if (int.TryParse(tmp[0], out score) && tmp.Length == 2)
                            {
                                results.Add(new ResultNode(score, tmp[1]));
                            }
                        }
                        sr.Close();
                    }
                }
                catch (Exception)
                {
                    Console.WriteLine("Chyba při čtení výsledků");
                }
                
            }            
            results.Sort();
        }

        private void StartGame(double difficulty)
        {
            game = new Game(GameCanvas, difficulty);
            timer = new DispatcherTimer();
            timer.Tick += game.Move;
            timer.Tick += RedrawLabels;
            timer.Tick += CheckEndGame;
            timer.Interval = TimeSpan.FromMilliseconds(10);
            timer.Start();
            GameCanvas.Focus();
        }

        private void GameCanvas_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.A)
            {
                game.Player.IsRotatingCounterClockwise = true;
            }
            if (e.Key == Key.D)
            {
                game.Player.IsRotatingClockwise = true;
            }
            if (e.Key == Key.W)
            {
                game.Player.Accelerate = true;
            }
            if (e.Key == Key.Space)
            {
                game.Player.IsFiring = true;
            }

        }

        private void GameCanvas_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.A)
            {
                game.Player.IsRotatingCounterClockwise = false;
            }
            if (e.Key == Key.D)
            {
                game.Player.IsRotatingClockwise = false;
            }
            if (e.Key == Key.W)
            {
                game.Player.Accelerate = false;
            }
            if (e.Key == Key.Space)
            {
                game.Player.IsFiring = false;
            }
        }

        private void CheckEndGame(object sender, EventArgs e)
        {
            if (game.GameOver)
            {
                timer.Stop();
                GameCanvas.Visibility = Visibility.Hidden;
                endGameGrid.Visibility = Visibility.Visible;
                scoreLabel.Content = game.Score.ToString();
                Keyboard.Focus(nicknameTextBox);
            }
        }

        private void RedrawLabels(object sender, EventArgs e)
        {
            LivesLabel.Content = "Lives: " + game.Player.Lives;
            ScoreLabel.Content = "Score: " + game.Score;
        }

        private void EasyDificultyLabel_MouseEnter(object sender, MouseEventArgs e)
        {
            easyDificultyLabel.Foreground = new SolidColorBrush(Colors.White);
        }

        private void EasyDificultyLabel_MouseLeave(object sender, MouseEventArgs e)
        {
            easyDificultyLabel.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFC07BFF"));
        }

        private void EasyDificultyLabel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            menuGrid.Visibility = Visibility.Hidden;
            GameCanvas.Visibility = Visibility.Visible;
            StartGame(0.75);
        }

        private void MediumDificultyLabel_MouseEnter(object sender, MouseEventArgs e)
        {
            mediumDificultyLabel.Foreground = new SolidColorBrush(Colors.White);
        }

        private void MediumDificultyLabel_MouseLeave(object sender, MouseEventArgs e)
        {
            mediumDificultyLabel.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFC07BFF"));
        }

        private void MediumDificultyLabel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            menuGrid.Visibility = Visibility.Hidden;
            GameCanvas.Visibility = Visibility.Visible;
            StartGame(1);
        }

        private void HardDificultyLabel_MouseEnter(object sender, MouseEventArgs e)
        {
            hardDificultyLabel.Foreground = new SolidColorBrush(Colors.White);
        }

        private void HardDificultyLabel_MouseLeave(object sender, MouseEventArgs e)
        {
            hardDificultyLabel.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFC07BFF"));
        }

        private void HardDificultyLabel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            menuGrid.Visibility = Visibility.Hidden;
            GameCanvas.Visibility = Visibility.Visible;
            StartGame(1.25);
        }



        private void ShowResultsLabel_MouseLeave(object sender, MouseEventArgs e)
        {
            showResultsLabel.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFC07BFF"));
        }

        private void ShowResultsLabel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ShowBestResults();
        }


        private void EndGameLabel_MouseEnter(object sender, MouseEventArgs e)
        {
            endGameLabel.Foreground = new SolidColorBrush(Colors.White);
        }

        private void EndGameLabel_MouseLeave(object sender, MouseEventArgs e)
        {
            endGameLabel.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFC07BFF"));
        }

        private void EndGameLabel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Environment.Exit(0);
        }

        private void ShowResultsLabel_MouseEnter(object sender, MouseEventArgs e)
        {
            showResultsLabel.Foreground = new SolidColorBrush(Colors.White);
        }


        private void SaveLabel_MouseEnter(object sender, MouseEventArgs e)
        {
            saveLabel.Foreground = new SolidColorBrush(Colors.White);
        }

        private void SaveLabel_MouseLeave(object sender, MouseEventArgs e)
        {
            saveLabel.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFC07BFF"));
        }

        private void SaveLabel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (nicknameTextBox.Text.Length ==0 || nicknameTextBox.Text == "--" || nicknameTextBox.Text == "---")
            {
                setNicknameLabel.Visibility = Visibility.Visible;
                return;
            }
            SaveResultToFile(game.Score, nicknameTextBox.Text);
            results.Add(new ResultNode(game.Score, nicknameTextBox.Text));
            results.Sort();
            ShowBestResults();
            nicknameTextBox.Text = "--";
            setNicknameLabel.Visibility = Visibility.Hidden;
        }

        private void DontSaveLabel_MouseEnter(object sender, MouseEventArgs e)
        {
            dontSaveLabel.Foreground = new SolidColorBrush(Colors.White);
        }

        private void DontSaveLabel_MouseLeave(object sender, MouseEventArgs e)
        {
            dontSaveLabel.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFC07BFF"));
        }

        private void DontSaveLabel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ShowBestResults();
            nicknameTextBox.Text = "--";
            setNicknameLabel.Visibility = Visibility.Hidden;
        }

        private void SaveResultToFile(int score, string nickname)
        {
            using (FileStream fs = new FileStream(@"results.csv", FileMode.Append, FileAccess.Write))
            {
                StreamWriter sw = new StreamWriter(fs);
                sw.WriteLine($"{score};{nickname}");
                sw.Flush();
                sw.Close();
            }
        }

        private void BackToMenuLabel_MouseEnter(object sender, MouseEventArgs e)
        {
            backToMenuLabel.Foreground = new SolidColorBrush(Colors.White);
        }

        private void BackToMenuLabel_MouseLeave(object sender, MouseEventArgs e)
        {
            backToMenuLabel.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFC07BFF"));
        }

        private void BackToMenuLabel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            bestResultsGrid.Visibility = Visibility.Hidden;
            menuGrid.Visibility = Visibility.Visible;
        }


        private void ShowBestResults()
        {
            bestResultsGrid.Visibility = Visibility.Visible;
            menuGrid.Visibility = Visibility.Hidden;
            endGameGrid.Visibility = Visibility.Hidden;
            bestResultsGrid.Children.Clear();
            bestResultsGrid.Children.Add(bestResultsLabel);
            Grid.SetRow(bestResultsLabel, 0);
            Grid.SetColumn(bestResultsLabel, 0);
            Grid.SetColumnSpan(bestResultsLabel, 2);

            bestResultsGrid.Children.Add(backToMenuLabel);
            Grid.SetRow(backToMenuLabel, 11);
            Grid.SetColumn(backToMenuLabel, 0);
            Grid.SetColumnSpan(backToMenuLabel, 2);

            for (int i = 1; i < Math.Min(11, results.Count+1); i++)
            {
                GenerateBestResultRow(i, results[i-1]);
            }
            if (results.Count < 10)
            {
                for (int i = results.Count+1; i < 11; i++)
                {
                    GenerateBestResultRow(i, ResultNode.GenerateDefaultNode());
                }
            }
        }

        private void GenerateBestResultRow(int row, ResultNode node) {
            Label nicknameLabel = new Label
            {
                Content = node.Nickname,
                Background = new SolidColorBrush(Colors.Transparent),
                Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFC07BFF")),
                HorizontalContentAlignment = HorizontalAlignment.Left,
                FontSize = 20
            };

            bestResultsGrid.Children.Add(nicknameLabel);
            Grid.SetRow(nicknameLabel, row);
            Grid.SetColumn(nicknameLabel, 0);

            Label scoreLabel = new Label
            {
                Content = node.Score,
                Background = new SolidColorBrush(Colors.Transparent),
                Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFC07BFF")),
                HorizontalContentAlignment = HorizontalAlignment.Right,
                FontSize = 20
            };

            bestResultsGrid.Children.Add(scoreLabel);
            Grid.SetRow(scoreLabel, row);
            Grid.SetColumn(scoreLabel, 1);
        }

    }
}
