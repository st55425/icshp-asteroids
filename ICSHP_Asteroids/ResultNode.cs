﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICSHP_Asteroids
{
    class ResultNode : IComparable
    {
        public int Score { get;private set; }
        public string Nickname { get; private set; }

        public ResultNode(int score, string nickname)
        {
            Score = score;
            Nickname = nickname;
        }

        public int CompareTo(object obj)
        {
            if (obj.GetType() == typeof(ResultNode))
            {
                ResultNode node = obj as ResultNode;
                return node.Score.CompareTo(Score);
            }
            throw new ArgumentException();
        }

        public static ResultNode GenerateDefaultNode()
        {
            return new ResultNode(0, "---");
        }
    }
}
